<?php

session_start();

header("Content-type: text/html; charset=UTF-8");
require_once('config.php');
require_once('functions.php');
//require_once('movieGetter.php');
//require_once('displayUserPage.php');

function h4($s) {
  return htmlspecialchars($s, ENT_QUOTES, "UTF-8");
}

if (!isAjax()){
  //不正なアクセスを禁止
  header('Location: '.SITE_URL);
  exit;
}

function isYoutube($input){
  // 入力された文字列がyoutubeを含むならtrue
  $checker = 'youtube';
  $pos = strpos($input, $checker);
  if ($pos === false){
    return false;
  } else {
    return true;
  }
}

$userId = (int)$_SESSION['id'];
// echo 'START:::userId='.$userId.'<br />';

if (isset($_POST['confirmed']) && isset($_POST['videoId'])){
  $dbh = connectDb();
  if (!isYoutube($_POST['videoId'])){
    echo 'Youtubeじゃないよ';
  }

  $videoId = substr($_POST['videoId'], 8, strlen($_POST['videoId']));
  $videoUrl = 'http://www.youtube.com/watch?v='.$videoId;

  $sql = "delete from user_pref where videourl = :videourl and user_id = :user_id";
  $stmt = $dbh->prepare($sql);
  $params = array(
    ':user_id' => $userId,
    ':videourl' => $videoUrl
  );
  $stmt->execute($params);

  echo '削除しました';

} else {
  echo '入力が不正です';
}
?>

