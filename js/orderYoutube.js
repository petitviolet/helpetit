j = jQuery.noConflict(); // めっちゃ大事
// 動画の表示を変更してからjsを再読込する
function require( uri ) {
  var script  = document.createElement( 'script' );
  script.type = 'text/javascript';
  var now = new Date();
  var hour = now.getHours();
  var min = now.getMinutes();
  var sec = now.getSeconds();
  var temp = hour.toString() + min.toString() + sec.toString()
  script.src  = uri + '?v=' + temp;
  var head    = document.getElementsByTagName( 'head' )[0];
  head.appendChild( script );
}
j(function() {
  j('div.header li > a').addClass('headerLink');

  j('#orderYoutube').click(function() {
    // 並び替えてる間は並び替えボタン押せなくする
    j('span.nowOrdering').show(500).text('並び替え中...');
    j(this).attr("disabled","disabled");
    var order = j('#selectYoutube').val();
    j.ajax({
      type: 'post',
      //並べ替えるsql叩くphp
      url: 'orderYoutube.php',
      data: {
        // select要素からtotalかmonthかを取得
        'order': order
      },
      success: function(data) {
        // 並び替えたものをhtmlで出力
        j('#youtube').html(data);
        switch (order) {
          case 'total':
            j('#movieTag').text('総合人気');
            break;
          case 'month':
            j('#movieTag').text('今月の人気');
            break;
          case 'random':
            j('#movieTag').text('ランダム表示');
            break;
          case 'new':
            j('#movieTag').text('新着動画');
            break;
        }
        // 並び替えメッセージの消去とボタンの有効化
        j('span.nowOrdering').hide(500);
        require('./js/flexcroll/flexcroll.js');
        require('./js/videobox.js');
        j('#orderYoutube').removeAttr("disabled");
        // console.log('ok:' + data);
      },
      // エラー処理らしい
      error: function(XMLHttpRequest, textStatus, errorThrown)
      {
        alert('Error : ' + errorThrown);
      },
      // 返ってくるデータはhtml形式
      dataType: 'html'
    });
    // クリックを無効化
    return false;
  });

  j('img').hide(); //ページ上の画像を隠す
  var i = 0;
  var interbal = 0;
  j(window).bind('load', function() { //ページコンテンツのロードが完了後、機能させる
    interbal = setInterval(function() {
      var images = j('img').length; //画像の数を数える
      if (i >= images) { // ループ
        clearInterval(interbal); //最後の画像までいくとループ終了
      }
      j('img:hidden').eq(0).fadeIn(200); //一つずつ表示する
      i++;
    }, 200);
  });

});
