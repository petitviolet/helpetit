// userPage.phpについての表示とかUI設定
j = jQuery.noConflict(); // めっちゃ大事

// 動画の表示を変更してからjsを再読込する
function require( uri ) {
  var script  = document.createElement( 'script' );
  script.type = 'text/javascript';
  var now = new Date();
  var hour = now.getHours();
  var min = now.getMinutes();
  var sec = now.getSeconds();
  var temp = hour.toString() + min.toString() + sec.toString()
  script.src  = uri + '?v=' + temp;
  var head    = document.getElementsByTagName( 'head' )[0];
  head.appendChild( script );
}

j(function() {
  j('div.header li > a').addClass('headerLink');
  // 動画が何も無い時
  if (j('#contents>.videoContent').length == 0) {
    j('#contents').html('上のボタンから動画を追加しましょう！').css({
      padding: '10px',
      color: '#CC3333',
      'font-size': '30px'
    });
  }

  /*
     var htmlinfo = function(){
     var info = {};
     if (window.innerWidth) {
     info.height = window.innerHeight;
     info.width = window.innerWidth;
     info.scrolly = window.pageYOffset;
     }
     else {
     if (document.documentElement && document.documentElement.clientWidth) {
     info.height = document.documentElement.clientHeight;
     info.width = document.documentElement.clientWidth;
     info.scrolly = document.documentElement.scrollTop;
     }
     else {
     if (document.body.clientWidth) {
     info.height = document.body.clientHeight;
     info.width = document.body.clientWidth;
     info.scrolly = document.body.scrollTop;
     }
     }
     }
     return info;
     };

     document.on('click', 'img', function(){
     this.css({
width: htmlinfo.width,
heigth: htmlinfo.width * 0.75
});
});
*/


  // ランダム表示を一度でもしたら並び替えを保存しない
  // false でランダム表示していない状態、 true でランダム表示した状態
  var randomFlag = false;

  function updateSeq(newVideoContents) {
    if (!randomFlag) {
      // 並べかえをデータベースに反映
      j.ajax({
        type: 'post',
        async: false,
        url: './updateSeq.php',
        data: {
          'newVideoContents': newVideoContents
        },
        success: function(data) {
          // console.log('success!:' + data);
        },
        // エラー処理らしい
        error: function(XMLHttpRequest, textStatus, errorThrown)
        {
          alert('うまくいかず:' + errorThrown + ', ' + textStatus + ', ' + event.returnValue);
        },
        // 返ってくるデータはhtml形式
        dataType: 'html'
      });
    }
  }

  var newVideoContents = [];
  // 動画コンテンツの入れ替え可能化設定
  window.onbeforeunload = function(event) {
    // console.log('length:', newVideoContents.length, ', ', newVideoContents);
    if (newVideoContents.length > 1) { // 動画1つならsortする必要なし
      updateSeq(newVideoContents);
    }
    // return true;
    // return でページ更新時にアラートが出てしまう
  };

  var Contents = j('#contents');

  Contents.sortable();
  Contents.sortable({
    stop: function() {
      // 全てのvideoContentについてseqの更新
      var videoContents = j('div.videoContent');
      var videoLength = videoContents.length;
      newVideoContents = []; // 初期化
      // newVideoContents.length = 0; // 初期化できるらしい
      for (var i = 0; i < videoLength; i++) {
        var contentId = videoContents.children('span').children('div').eq(i).attr('id');
        newVideoContents[i] = contentId;
      }
      // console.log('length:', newVideoContents.length, ', ', newVideoContents);
    },
    placeholder: 'placeholder',
    helper: 'clone',
    forcePlacehoderSize: true,
    opacity: 0.7
  });

  // 動画追加のtextボックスのヒント設定
  // 無くなった
  var addMovie = j('#addMovieUrl');
  var defautText = '追加したい動画のURLを入力してください';

  if (addMovie.val() == '' || addMovie.val() == defautText) {
    addMovie.val(defautText).addClass('initHint');
  }
  addMovie
  .focus(function() {
    if (j(this).val() == defautText) {
      j(this).val('').removeClass('initHint').addClass('afterHint');
    }
  });
  addMovie
  .blur(function() {
    if (addMovie.val() == '' || addMovie.val() == defautText) {
      j(this).val(defautText).removeClass('afterHint').addClass('initHint');
    }
  });
  // ここまでいらない(ヒント)

  // 動画追加ダイアログ設定
  // j('#addMovieButton').button();
  j('#addMovieButton').click(function() {
    j('#addMovieDialog').dialog('open');
    return false;
  });

  function reDisplay() {
    j.ajax({
      type: 'post',
      url: './reDisplayUserPage.php',
      data: {
        'reDisplay': true
      },
      success: function(data) {
        j('#contents').html(data);
        require('./js/flexcroll/flexcroll.js');
        require('./js/videobox.js');
      },
      // エラー処理らしい
      error: function(XMLHttpRequest, textStatus, errorThrown)
      {
        //j('span.nowAdding').text('URLをお確かめください');
        alert('URLをお確かめください');
        j('span.nowAdding').hide(5000);
        //alert('Error : ' + errorThrown);
        j('button.ui-button').button('enable');
      },
      // 返ってくるデータはhtml形式
      dataType: 'html'
    });
  }

  j('#addMovieDialog').dialog({
    // 透けるのはcssのz-indexを設定して克服
    autoOpen: false,
    title: '動画を追加します',
    show: 'blind',
    hide: 'blind',
    width: 500,
    heigth: 'auto',
    closeOnEscape: true,
    draggable: true,
    resizeble: true,
    modal: true,
    closeText: 'Cancel',
    position: 'center',
    focus: function() {
      j('#addDialogUrl').focus();
    },
    open: function() {
      // なぜか裏の並び替えが出来てしまうので無効化
      j('#contents').sortable('disable');
    },
    close: function() {
      // 並び替え有効化
      j('#contents').sortable('enable');
      j('span.nowAdding').hide(2000);
    },
    buttons: {
      '追加': function() {
        j('span.nowAdding').show(500).text('追加中...');
        j('button.ui-button').button('disable');
        j.ajax({
          type: 'post',
          url: './addMovie.php',
          data: {
            'movieUrl': j('#addDialogUrl').val(),
            'memo': j('#addDialogMemo').val()
          },
          success: function(data) {
            j('span.nowAdding').html(data);
            //j(this).removeAttr('disabled');
            j('button.ui-button').button('enable');
            if (j('#addDialogUrl').val() != '') {
              j('#addMovieDialog').dialog('close');
              // console.log('OK');
            } else {
              alert('URLが入力されていません');
              j('span.nowAdding').hide(5000);
            }
            reDisplay();
            j('#addDialogUrl').val(''),
            j('#addDialogMemo').val('')
          },
          // エラー処理らしい
          error: function(XMLHttpRequest, textStatus, errorThrown)
          {
            //j('span.nowAdding').text('URLをお確かめください');
            alert('URLをお確かめください');
            j('span.nowAdding').hide(5000);
            //alert('Error : ' + errorThrown);
            j('button.ui-button').button('enable');
          },
          // 返ってくるデータはhtml形式
          dataType: 'html'
        });
      },
      'キャンセル': function() {
        j(this).dialog('close');
      }
    }
  });
  // 動画追加ダイアログ周りここまで

  // サムネイルあたりをダブルクリックで削除ボタン登場
  // ボタンを押すとダイアログで確認
  var deleteButtonHtml = "<div id='deleteButton'><button id='deleteMovieButton' value='test'>動画を削除</button>  <span id='deleteMovieDialog' title='削除しますか？'><span class='nowDeleting'></span></span></div>";
  var flag = false; // ダブルクリックしたものがあるか。false:ない, true:ある

  // 別にj(document).on(~)を使う必要はない
  j(document).on('dblclick', '.videoContent', function() {
    if (!flag) {
      // 一度ダブルクリックして削除ボタンが出現していると
      // 他の動画をダブルクリックすると先程の削除ボタンが消えて
      // ダブルクリックしたところに新たに削除ボタンが登場
      // buttonPlaceとmemoはグローバル関数としてエスケープ
      buttonPlace = j(this).find('.videoCaption');
      buttonPlaceParent = j(this);
      memo = buttonPlace.html();
      buttonPlace.slideUp(200, function() {
        buttonPlace.html(deleteButtonHtml);
      }).slideDown(200);
      //buttonPlaceParent.css({'position': 'relative', 'top': '-4em'});
      flag = true;
    } else {
      // 削除ボタンが既に登場している時
      // ダブルクリックしたところが削除ボタンが存在しているところと同じかどうか
      var clickedPlace = j(this).find('.videoCaption');
      var clickedPlaceParent = j(this);
      if (clickedPlace.html() == buttonPlace.html()) {
        // 同じなら削除ボタンを消す
        // buttonPlace.html(memo);
        buttonPlace.slideUp(200, function() {
          buttonPlace.html(memo);
          //buttonPlaceParent.css({'position': 'relative', 'top': '0'});
        }).slideDown(200);
        flag = false;
      } else {
        // 違うなら削除ボタンを消して新たに登場させる
        buttonPlace.slideUp(200, function() {
          buttonPlace.html(memo);
          //buttonPlaceParent.css({'position': 'static'});
          // memoの更新操作をこの外にだすと、
          // アニメーションしてる間に値が変わって変な動作になる
          memo = clickedPlace.html();
          buttonPlace = clickedPlace;
        }).slideDown(200);
        clickedPlace.slideUp(200, function() {
          clickedPlace.html(deleteButtonHtml);
          //clickedPlaceParent.css({'position': 'relative', 'top': '-4em'});
        }).slideDown(200);
        flag = true;
      }
    }
  });

  // 動画削除ダイアログ設定
  // ダブルクリックで登場した削除ボタンにdialog機能を持たせるために
  // j(document).on(event, selector, fn)を利用
  j(document).on('click', '#deleteMovieButton', function() {
    j('#deleteMovieDialog').dialog({
      // 透けるのはcssのz-indexを設定して克服
      autoOpen: false,
      title: '動画を削除しますか?',
      show: 'blind',
      hide: 'blind',
      width: 500,
      heigth: 'auto',
      closeOnEscape: true,
      draggable: true,
      resizeble: true,
      modal: true,
      closeText: 'Cancel',
      position: 'center',
      open: function() {
        // なぜか裏の並び替えが出来てしまうので無効化
        j('#contents').sortable('disable');
      },
      close: function() {
        // 並び替え有効化
        j('#contents').sortable('enable');
        j('span.nowDeleting').hide(500);
        buttonPlace.slideUp(500, function() {
          j(this).html(memo);
        }).slideDown(500);
        flag = false;
      },
      buttons: {
        '削除': function() {
          j('span.nowDeleting').show(500).text('削除中...');
          j('button.ui-button').button('disable');
          var target = buttonPlace.parent('.videoContent');
          var videoId = target.children('span').children('div').attr('id');
          j.ajax({
            type: 'post',
            //実装しましょう
            url: './deleteMovie.php',
            data: {
              'videoId': videoId,
              'confirmed': true
            },
            success: function(data) {
              j('span.nowDeleting').html(data);
              //j(this).removeAttr('disabled');
              j('button.ui-button').button('enable');
              j('#deleteMovieDialog').dialog('close');
              // buttonPlaceは削除ボタンのある場所
              // console.log(videoId);
              // console.log(data);
              target.css({border: '1px solid #FFFFFF'});
              target.hide(1000, function() {
                target.remove(); //画面からゆっくり削除
                if (j('#contents>.videoContent').length == 0) {
                  j('#contents').html('動画を追加しましょう！').css({
                    padding: '10px',
                    color: '#FF0000'
                  });
                }
              });
            },
            // エラー処理らしい
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
              //j('span.nowAdding').text('URLをお確かめください');
              alert('削除に失敗しました');
              j('span.nowDeleting').hide(500);
              //alert('Error : ' + errorThrown);
              j('button.ui-button').button('enable');
            },
            // 返ってくるデータはhtml形式
            dataType: 'text'
          });
        },
        'キャンセル': function() {
          j(this).dialog('close');
        }
      }
    });
    j('#deleteMovieDialog').dialog().dialog('open');
    return false;
    //j(document).on('click', '#deleteMovieButton', function() {
  }); //ここまで

  // ランダム表示関係
  j(document).on('click', '#randomButton', function() {
    // 並び替えてる間は並び替えボタン押せなくする
    // ランダム表示しましたよフラグ更新
    randomFlag = true;
    j('span.nowOrdering').show(500).text('並び替え中...');
    j('#randomButton').attr('disabled', 'disabled');
    j.ajax({
      type: 'post',
      //並べ替えるsql叩くphp
      url: 'randomSort.php',
      data: {
        // select要素からtotalかmonthかを取得
        // 'order': j('#selectYoutube').val()
        'data': true
      },
      success: function(data) {
        // 並び替えたものをhtmlで出力
        j('#contents').html(data);
        require('./js/flexcroll/flexcroll.js');
        require('./js/videobox.js');
        // 並び替えメッセージの消去とボタンの有効化
        j('span.nowOrdering').hide(500);
        j('#randomButton').removeAttr('disabled');
        // console.log('randomSort is OK');
      },
      // エラー処理らしい
      error: function(XMLHttpRequest, textStatus, errorThrown)
      {
        alert('Error : ' + errorThrown);
      },
      // 返ってくるデータはhtml形式
      dataType: 'html'
    });
    // クリックを無効化
    return false;
  });



  // 画像の読み込みを遅らせる
  /*
     var preLoad = function (url) {
     j('img')
     .hide()
     .attr('src', url)
     .appendTo(document.body)
     .load(function () { j(this).remove(); })
     .error(function () { j(this).remove(); });
     }
     */

  j('contents img').hide(); //ページ上の画像を隠す
  var i = 0;
  var interbal = 0;
  j(window).bind('load', function() { //ページコンテンツのロードが完了後、機能させる
    interbal = setInterval(function() {
      var images = j('img').length; //画像の数を数える
      if (i >= images) { // ループ
        clearInterval(interbal); //最後の画像までいくとループ終了
      }
      j('img:hidden').eq(0).fadeIn(200); //一つずつ表示する
      i++;
    }, 200);
  });
});

