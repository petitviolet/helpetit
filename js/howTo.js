j = jQuery.noConflict(); // めっちゃ大事
j(function() {
  j('div.header li > a').addClass('headerLink');

  j(document).on('click', '#exitLink', function(){
    if(window.confirm('アカウントを削除します。よろしいですか？')) {
      location.href = "deleteAccount.php";
    }
  });
});
