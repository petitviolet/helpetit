<?php

session_start();

header("Content-type: text/html; charset=UTF-8");
require_once('config.php');
require_once('functions.php');

// アカウントを削除するユーザーのID
$forDelete = (int)$_SESSION['forDelete'];
unset($_SESSION['forDelete']); // 念のため
$params = array(
  ':userId' => $forDelete,
);

$dbh = connectDb();

$sql = "delete from user_pref where user_id = :userId";
$stmt = $dbh->prepare($sql);
$stmt->execute($params);

$sql = "delete from users where id = :userId";
$stmt = $dbh->prepare($sql);
$stmt->execute($params);

echo '削除しました';

$_SESSION = array();

if (isset($_COOKIE[session_name()])){
  setcookie(session_name(), '', time()-86400, '/');
}

session_destroy();

header('Location: '.SITE_URL.'login.html');
exit;

?>


