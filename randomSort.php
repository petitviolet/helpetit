<?php
session_start();
define('Charset', 'UTF-8');	// 出力する文字コードの定義

require_once('config.php');
require_once('functions.php');
require_once('movieGetter.php');
require_once('displayUserPage.php');

if (!isAjax()){
  //不正なアクセスを禁止
  header('Location: '.SITE_URL);
  exit;
}

$userId = (int)$_SESSION['id'];

if (isset($_POST['data']) && $_POST['data'] !== true){
  $sql = 'select * from user_pref where user_id = :id order by rand() limit 30';
  $dbh = connectDb();
  $stmt = $dbh->prepare($sql);
  $stmt->execute(array(":id"=>$userId));
  /*
  while ($row = $stmt->fetch()){
    print_r($row);
    print_r('<br />');
  }
   */
  $rows = $stmt->fetchAll();
  // shuffle($rows);
  $userPrefs = getUserPrefs($rows);
  $contents = addTags($userPrefs);
  echo $contents;
} else {
  echo 'なにかがおかしいよ！';
}
