<?php
// 純正のvideobox.jsに、クリックイベントとして
// 再生回数を増やすpostを追加した

// ajax(post)を行う関数。
// 引数に"http://www_youtube_com/watch?v"をkeyとして
// videoIdをvalueとする配列を渡す

session_start();

header("Content-type: text/html; charset=UTF-8");
require_once('config.php');
require_once('functions.php');

// isAjax()はfalseになっちゃう
// isset($_POST[$usl])でごまかす
// if (!isAjax()){
//   //不正なアクセスを禁止
//   header('Location: '.SITE_URL);
//   exit;
// }
// 動画をクリックしたときに再生回数を増やす

$userId = (int)$_SESSION['id'];
// $_POST["http://www_youtube_com/watch?v"]にvideoIdが格納
$url = "http://www_youtube_com/watch?v";

// if (isset($_POST['confirmed']) && isset($_POST['videoId'])){
if (isset($_POST[$url])){
  $videoId = $_POST[$url];
  unset($_POST[$url]); //初期化してみる
  $dbh = connectDb();
  // $videoId = substr($_POST['videoId'], 8, strlen($_POST['videoId']));
  // 月間、合計再生回数をそれぞれ1増やす
  $sql = "update youtube set month = month + 1 ,total = total + 1 ,modified = now() where videoid = :videoid";
  $stmt = $dbh->prepare($sql);
  $params = array(':videoid' => $videoId);
  $stmt->execute($params);
  echo '無事に再生回数を増やしました';
} else {
  echo 'postできてないです';
  header('Location: '.SITE_URL);
  exit;
}
?>
