<?php
session_start();
define('Charset', 'UTF-8');	// 出力する文字コードの定義

require_once('config.php');
require_once('functions.php');
require_once('displayUserPage.php');

if (!isAjax()){
  //不正なアクセスを禁止
  header('Location: '.SITE_URL);
  exit;
}
if (empty($_SESSION['me'])) {
  header('Location: '.SITE_URL.'login.php');
  exit;
}
if (isset($_POST['reDisplay']) && $_POST['reDisplay'] == true){
  $userId = $_SESSION['id'];
  $contents = getUserPage($userId);
  echo $contents;
} else {
  echo $_POST['reDisplay'];
  echo ':おかしいよ〜';
}
