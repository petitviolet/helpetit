<?php

session_start();

header("Content-type: text/html; charset=UTF-8");

require_once('config.php');
require_once('functions.php');

if (!isAjax()){
  //不正なアクセスを禁止
  header('Location: '.SITE_URL);
  exit;
}

$userId = (int)$_SESSION['id'];
echo 'updateSeq, START:::userId='.$userId.'<br />';

if (isset($_POST['newVideoContents']) && $_POST['newVideoContents'] != array()){
  $dbh = connectDb();
  $sql = 'update user_pref set seq = :seq, modified = now() where user_id = :user_id and videourl = :videourl';
  $newVideoContents = $_POST['newVideoContents'];
  // $stmt = $dbh->prepare($sql);

  foreach ($newVideoContents as $updateVideoSeq => $videoId) {
    echo $updateVideoSeq;
    $videoId = substr($videoId, 8, strlen($videoId));
    $videoUrl = 'http://www.youtube.com/watch?v='.$videoId;
    $updateVideoSeq = $updateVideoSeq + 1;
    $params = array(
      ':seq' => $updateVideoSeq,
      ':user_id' => $userId,
      ':videourl' => $videoUrl
    );
    $stmt = $dbh->prepare($sql);
    $stmt->execute($params);
    // $stmt->bind_param($params);
    // $stmt->execute();
  }
  echo 'updateが完了しました';
} else {
  echo '入力がおかしいです';
}
?>


