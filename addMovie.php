<?php

session_start();

header("Content-type: text/html; charset=UTF-8");
require_once('config.php');
require_once('functions.php');

function h3($s) {
  return htmlspecialchars($s, ENT_QUOTES, "UTF-8");
}

if (!isAjax()){
  //不正なアクセスを禁止
  header('Location: '.SITE_URL);
  exit;
}

function CheckUrl($checkurl){
  // urlが正しい形式であればtrue
  if(preg_match('/^(http|https):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $checkurl)){
    return true;
  }
  return false;
}

// youtubeのurlからタイトルを取得
function getYoutubeTitle($youtubeUrl){
  //正規表現
  if (!CheckUrl($youtubeUrl)) {
    return false;
  }
  $pattern = "<title>(.*)<\/title>";
  $youtubeUrl = h3($youtubeUrl);
  $html = file($youtubeUrl);
  $str = implode("", $html);
  if(preg_match("/".$pattern."/i", $str, $match)){
    //UTF-8からShift-JISへ変換する。
    //$string  = mb_convert_encoding($match[0],"SJIS", "UTF-8");
    //$string2 = mb_convert_encoding($match[1], "SJIS", "UTF-8");
    $string2 = mb_convert_encoding($match[1], "UTF-8");
  }
  $titleStr = str_replace("- YouTube", "", $string2);
  return $titleStr;
}

function extractVideoId($videourl){
  //ニコ動
  //if (preg_match('/http:\/\/www.nicovideo.jp\/watch\/[a-zA-Z0-9]+/u',$videourl)){
  //  $nico_id=substr($videourl,30,10); //31文字目から10桁の文字を取得
  //  $text = $nico_id;
  //}
  //以下YouTube（2パターン）
  if (preg_match('/http:\/\/www.youtube.com\/watch/u',$videourl)){
    if(strpos($videourl,'watch?v=')===false){//余計なパラメータが付いている場合
      $youtube_id=substr($videourl,-11,11); //パラメータがない場合は後ろから11桁
    }else{
      $youtube_id=substr($videourl,31,11); //パラメータがある場合は頭から数える
    }
    $text = $youtube_id;
  }

  //短縮URLの場合
  if(preg_match('/h?ttp:\/\/youtu.be\//u',$videourl)){
    $youtube_id=substr($videourl,-11,11); //後ろから11桁を取得
    $text = $youtube_id;
  }
  return $text;
}

function checkExistUrl($videoId){
  // すでに存在する動画ならばfalse
  $dbh = connectDb();
  $sql = "select * from youtube where videoid = :videoid";
  // $dbh->query($sql);
  // $result = $dbh->fetchAll();
  $stmt = $dbh->prepare($sql);
  $params = array(":videoid" => $videoId);
  $stmt->execute($params);
  $result = $stmt->fetchAll();
  if (!$result){
    return true;
  } else {
    return false;
  }
}

function checkExistPref($userId, $movieUrl){
  // その動画をすでにuserが登録していればfalse
  $dbh = connectDb();
  $sql = 'select * from user_pref where user_id = :user_id and videourl = :videourl limit 1';
  $stmt = $dbh->prepare($sql);
  $params = array(
    ":user_id" => $userId,
    ":videourl" => $movieUrl
  );
  $stmt->execute($params);
  $result = $stmt->fetchAll();
  if (!$result) {
    return true;
  } else {
    return false;
  }
}

$userId = (int)$_SESSION['id'];
// echo 'START:::userId='.$userId.'<br />';
$dbh = connectDb();

if (isset($_POST['movieUrl']) && $_POST['movieUrl'] != ''){
  // XSS対策
  $movieUrl = h3($_POST['movieUrl']);
  // echo 'movieUrlは'.$movieUrl.'です。:::';

  $youtubeId = extractVideoId($movieUrl);
  if (!$youtubeId) {
    echo 'YouTubeのURLを入力してください';
    exit;
  }
  $youtubeTitle = getYoutubeTitle($movieUrl);
  $youtubeTitle = mb_convert_encoding($youtubeTitle, "UTF-8");
  if (!$youtubeTitle){
    echo 'URLがおかしいです';
    exit;
  }


  if (checkExistUrl($youtubeId)) {
    // 動画がyoutubeに登録されていなければ登録
    $sql = "insert into youtube (videoid, title, month, total, created, modified) values (:videoid, :title, :month, :total, now(), now())";
    $stmt = $dbh->prepare($sql);
    $params = array(
      ':videoid' => $youtubeId,
      ':title' => $youtubeTitle,
      ':month' => 1,
      ':total' => 1
    );
    $result = $stmt->execute($params);
  }

  if (checkExistPref($userId, $movieUrl)) {
    // この動画をuser_prefに登録していなければ登録
    $sql = "select max(seq) from user_pref where user_id =".$userId;
    $stmt = $dbh->query($sql);
    $newSeq = $stmt->fetch();
    $maxSeq = $newSeq['max(seq)'] + 1;
    $newRate = 3;
    // XSS対策
    $memo = h3($_POST['memo']);
    $sql = "insert into user_pref (user_id, videourl, seq, rate, memo, created, modified) values (:user_id, :videourl, :seq, :rate, :memo, now(), now())";
    $stmt = $dbh->prepare($sql);
    $params = array(
      ':user_id' => $userId,
      ':videourl' => $movieUrl,
      ':seq' => $maxSeq,
      ':rate' => $newRate,
      ':memo' => $memo
    );
    $stmt->execute($params);
    echo '無事に完了しました';
  } else {
    echo 'すでに登録されています';
  }
} else {
  echo 'URLが入力されていません';
}
?>
