<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>YouTube API</title>
  </head>
  <body>
    <?php
    $feedURL = "http://gdata.youtube.com/feeds/api/videos?vq=wordpress&max-results=5&start-index=1";
    $sxml = simplexml_load_file($feedURL);
    foreach($sxml->entry as $entry){
    $media = $entry->children('http://search.yahoo.com/mrss/');
    // タイトル
    $title = $entry->title;
    // 詳細
    $desc = $media->group->description;
    // URL
    $attrs = $media->group->player->attributes();
    $watch = $attrs['url'];
    // サムネイル
    $content = $media->group->content->attributes();
    $attrs = $media->group->thumbnail[0]->attributes();
    $thumbnail = $attrs['url'];
    // 動画の時間 (秒)
    $yt = $media->children('http://gdata.youtube.com/schemas/2007');
    $attrs = $yt->duration->attributes();
    $length = $attrs['seconds'];
    // 再生回数
    $yt = $entry->children('http://gdata.youtube.com/schemas/2007');
    $attrs = $yt->statistics->attributes();
    $viewCount = $attrs['viewCount'];
    // 評価
    $gd = $entry->children('http://schemas.google.com/g/2005');
    if ($gd->rating) {
    $attrs = $gd->rating->attributes();
    $rating = $attrs['average'];
    } else {
    $rating = 0;
    }
    // ID
    $arr = explode('/',$entry->id);
    $id = $arr[count($arr)-1];
    ?>
    <p>タイトル：<?php echo $title; ?></p>
    <p>詳細：<?php echo $desc; ?></p>
    <p>URL：<?php echo $watch; ?></p>
    <p>時間：<?php echo $length; ?>秒</p>
    <p>再生回数：<?php echo $viewCount; ?></p>
    <p>評価：<?php echo $rating; ?></p>
    <p>ID：<?php echo $id; ?></p>
    <a href="<?php echo $content -> url; ?>"><img src="<?php echo $thumbnail; ?>" /></a>
    <hr />
    <?php
    }
    ?>
  </body>
</html>
