<?php
session_start();
define('Charset', 'UTF-8');	// 出力する文字コードの定義
header("Content-type: text/html; charset=UTF-8");
require_once('config.php');
require_once('functions.php');
require_once('movieGetter.php');
require_once('displayUserPage.php');


if (empty($_SESSION['me'])) {
  header('Location: '.SITE_URL.'login.html');
  exit;
}

$me = $_SESSION['me'];

$dbh = connectDb();

// ログインユーザーのidとアクセスしたユーザーページのidが一致するかチェック
// $sql = 'select id from users where name = :name and email = :email limit 1';
$sql = 'select tw_user_id from users where tw_access_token = :tw_access_token and tw_access_token_secret = :tw_access_token_secret limit 1';
$stmt = $dbh->prepare($sql);
// $stmt->execute(array(":name"=>$me['name'], ":email"=>$me['email']));
$stmt->execute(array(":tw_access_token"=>$me['tw_access_token'], ":tw_access_token_secret"=>$me['tw_access_token_secret']));
$user = $stmt->fetch();

if ($user['tw_user_id'] != h((int)$_GET['id'])){
  header('Location: '.SITE_URL.'index.html');
  exit;
}

// 開いたユーザーページが存在していない場合(必要ないかも)
$sql = 'select * from users where tw_user_id = :id limit 1';
$stmt = $dbh->prepare($sql);
$stmt->execute(array(":id" => (int)$_GET['id']));
$user = $stmt->fetch();
if (!$user){
  echo 'no such user!!!';
  exit;
}

$_SESSION['id'] = $user['id'];

// ユーザーidを用いてそのユーザーの情報をデータベースから取得
$userId = $user['id'];
$contents = getUserPage($userId);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Expires" content="864000">
  <title>helpetit -ユーザーページ-</title>
  <link type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css" rel="stylesheet" />
  <link type="text/css" href="css/userPage.css" rel="stylesheet" />
  <link type="text/css" href="css/header.css" rel="stylesheet" />
  <link type="text/css" href="css/flexcroll.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/videobox.css" type="text/css" />

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  <script type="text/javascript" charset="utf-8" src="./js/mootools.js"></script>
  <script type="text/javascript" charset="utf-8" src="./js/swfobject.js"></script>
  <script type="text/javascript" charset="utf-8" src="./js/videobox.js"></script>
  <script type="text/javascript" charset="utf-8" src="./js/userPage.js"></script>
  <script type="text/javascript" charset="utf-8" src="./js/flexcroll/flexcroll.js"></script>

</head>
<body>
    <div class='header'>
      <div><a href='index.html'><img id='titleLogo' src="./img/helpetit.png"/></a></div>
      <div id="userNav">
        <ul>
          <li>
            <?php echo topPageLink($userId[0]); ?>
          </li>
          <li>
            <a id="howTo" href="howTo.html">helpetitについて</a>
          </li>
          <li>
            <a id='logoutButton' href="logout.php">ログアウト</a>
          </li>
        </ul>
      </div>
    </div>
<h2>ユーザーページ</h2>
<div class="addMovie">
  <button id='addMovieButton'>動画を追加</button>
  <span id='addMovieDialog' title='追加'>
    <p>
      <span>YouTubeのURL：<br /></span>
      <input type="text" id='addDialogUrl' value=''/>
    </p>
    <p>
      <span>メモ：<br /></span>
      <textarea id='addDialogMemo' value=''></textarea>
    </p>
  </span>
  <span class="nowAdding"></span>
</div>

<div id='random'>
  <button id='randomButton'>ランダム表示</button>
  <span class="nowOrdering"></span>
</div>

<div id="contents">
<?php echo $contents; ?>
</div>
<div id='toTop'>
  <a class='button-rink' href="index.html">トップページへ</a>
</div>
</body>
</html>
