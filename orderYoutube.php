<?php
header("Content-type: text/html; charset=UTF-8");
require_once('config.php');
require_once('functions.php');
require_once('movieGetter.php');

/*
function isAjax()
{
  if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
    return true;
  }
  return false;
}
 */
if (!isAjax()){
  //不正なアドレスです
  header('Location: '.SITE_URL.'login.php');
  exit;
}

if (isset($_POST['order'])){
  $order = $_POST['order'];
  $dbh = connectDb();
  if ($order === 'random') {
    $sqlYoutube = "select videoid, title from youtube order by rand() limit 50";
    $stmt = $dbh->prepare($sqlYoutube);
    $stmt->execute();
  } elseif ($order === 'new') {
    $order = 'created';
    $sqlYoutube = "select * from youtube order by $order desc limit 50";
    $stmt = $dbh->prepare($sqlYoutube);
    $params = array(':order' => $order);
    $stmt->execute($params);
  } else {
    $sqlYoutube = "select * from youtube order by $order desc limit 50";
    $stmt = $dbh->prepare($sqlYoutube);
    $params = array(':order' => $order);
    $stmt->execute($params);
  }
  while ($youtubeId = $stmt->fetch()) {
    // echo $youtubeId['id'].':'.$title.':'.$youtubeId[$order];
    // echo '<br />';
    $videoid = $youtubeId['videoid'];
    $title = $youtubeId['title'];
    $youtubeDiv = makeYoutubeDiv($videoid, $title);
    echo $youtubeDiv;
  }
} else {
  echo 'The parameter of "order" is not found';
}

?>
